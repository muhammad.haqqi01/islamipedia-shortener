export type Json =
  | string
  | number
  | boolean
  | null
  | { [key: string]: Json }
  | Json[]

export interface Database {
  public: {
    Tables: {
      hadith: {
        Row: {
          arab: string
          book: string
          id: string | null
          number: number
        }
        Insert: {
          arab: string
          book: string
          id?: string | null
          number: number
        }
        Update: {
          arab?: string
          book?: string
          id?: string | null
          number?: number
        }
      }
      quran: {
        Row: {
          id: number
          name: string
          total_verses: number
          translation: string
          transliteration: string
          type: string
          verses: Json[]
        }
        Insert: {
          id: number
          name: string
          total_verses: number
          translation: string
          transliteration: string
          type: string
          verses: Json[]
        }
        Update: {
          id?: number
          name?: string
          total_verses?: number
          translation?: string
          transliteration?: string
          type?: string
          verses?: Json[]
        }
      }
      url: {
        Row: {
          access_key: string
          url: string
          url_name: string
        }
        Insert: {
          access_key: string
          url?: string
          url_name?: string
        }
        Update: {
          access_key?: string
          url?: string
          url_name?: string
        }
      }
    }
    Views: {
      [_ in never]: never
    }
    Functions: {
      [_ in never]: never
    }
    Enums: {
      [_ in never]: never
    }
    CompositeTypes: {
      [_ in never]: never
    }
  }
}
