import { Database } from "./supabase";

export type ResData = {
  data?: any;
  message: string;
};

export type createURL = {
  access_key?: string;
  url: string;
  url_name: string;
};

export type URL = Database["public"]["Tables"]["url"]["Row"];

export type Quran = Database["public"]["Tables"]["quran"]["Row"]

export type Hadith = Database["public"]["Tables"]["hadith"]["Row"]