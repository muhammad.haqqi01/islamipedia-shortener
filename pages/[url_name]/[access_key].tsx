import { useRouter } from "next/router";
import { useEffect } from "react";

function RedirectWithKey() {
  const router = useRouter();

  useEffect(() => {
    const { url_name, access_key } = router.query;
    router.push(
      window.location.origin + "/api/shortener/" + url_name + "/" + access_key
    );
  }, [router.query]);
  return <div style={{ background: "black", height: "100vh" }}>Loading !</div>;
}

export default RedirectWithKey;
