import { useRouter } from "next/router";
import { useEffect } from "react";

function RedirectWithoutKey() {
  const router = useRouter();

  useEffect(() => {
    const { url_name } = router.query;
    router.push(window.location.origin + "/api/shortener/" + url_name);
  }, [router.query]);
  return <div style={{ background: "black", height: "100vh" }}>oading !</div>;
}

export default RedirectWithoutKey;
