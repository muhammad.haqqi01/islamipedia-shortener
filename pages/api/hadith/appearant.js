import { supabase } from "@/lib";

export default async function handler(req, res) {
  if (req.method === "GET") {
    //@ts-ignore
    const { word } = req.query;
    const sheets = await supabase
      .from("hadith")
      .select()
      .ilike("id", `%${word}%`);

    let occurence = sheets.data.length;
    res.status(sheets.status).json({
      message: `There are ${occurence} appearances in hadith`,
      data: sheets.data.splice(0, 101),
    });
  }
}
