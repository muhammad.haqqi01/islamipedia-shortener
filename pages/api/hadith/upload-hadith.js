import abudaud from "@/books/abu-daud.json";
import ahmad from "@/books/ahmad.json";
import bukhari from "@/books/bukhari.json";
import darimi from "@/books/darimi.json";
import ibnumajah from "@/books/ibnu-majah.json";
import malik from "@/books/malik.json";
import muslim from "@/books/muslim.json";
import nasai from "@/books/nasai.json";
import tirmidzi from "@/books/tirmidzi.json";
import { supabase } from "@/lib";

export default async function handler(req, res) {
  if (req.method === "GET") {
    let jsons = [
      abudaud,
      ahmad,
      bukhari,
      darimi,
      ibnumajah,
      malik,
      muslim,
      nasai,
      tirmidzi,
    ];
    let strings = [
      "abudaud",
      "ahmad",
      "bukhari",
      "darimi",
      "ibnumajah",
      "malik",
      "muslim",
      "nasai",
      "tirmidzi",
    ];

    for (let i = 0; i < jsons.length; i++) {
      const create = await supabase.from("hadith").insert(
        jsons[i].map((item) => {
          return { ...item, book: strings[i] };
        })
      );
    }
    res.status(201).json({ message: "Done", data: null });
  }
}
