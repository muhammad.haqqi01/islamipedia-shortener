import { supabase } from "@/lib";

export default async function handler(req, res) {
  if (req.method === "GET") {
    let strings = [
      "abudaud",
      "ahmad",
      "bukhari",
      "darimi",
      "ibnumajah",
      "malik",
      "muslim",
      "nasai",
      "tirmidzi",
    ];
    const random = strings[Math.floor(Math.random() * strings.length)];
    const sheets = await supabase
      .from("hadith")
      .select()
      .match({ book: random })

    const randomNumber = Math.floor(Math.random() * sheets.data.length);
    if (sheets.data) {
      sheets.data = sheets.data[randomNumber];
    }

    res
      .status(sheets.status)
      .json({ message: "Random hadith", data: sheets.data });
  }
}
