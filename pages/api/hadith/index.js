import { supabase } from "@/lib";

export default async function handler(req, res) {
  if (req.method === "GET") {
    const { book, number } = req.query;
    let sheets = undefined;

    if (book && number) {
      sheets = await supabase
        .from("hadith")
        .select()
        .match({ book: book, number: parseInt(number) })
        .single();
    } else if (book) {
      sheets = await supabase.from("hadith").select().match({ book: book });
    } else {
      res.status(500).json({
        message: `Fill atleast book name`,
        data: null,
      });
    }

    res.status(sheets.status).json({
      message: `Hadith found`,
      data: sheets.data,
    });
  }
}
