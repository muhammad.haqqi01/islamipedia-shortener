import { supabase } from "@/lib";
import { createURL, ResData } from "@/types";
import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<ResData>
) {
  if (req.method === "POST") {
    const data: createURL = req.body;

    try {
      const testURL = new URL(data.url);
    } catch (e) {
      res
        .status(500)
        .json({ message: "Please enter the valid URL", data: undefined });
      return;
    }

    if (!data.access_key) {
      data.access_key = "";
    }
    if (!data.url_name) {
      res
        .status(500)
        .json({ message: "Please enter the valid name", data: undefined });
      return;
    }
    const create = await supabase.from("url").insert(data).select().single();
    const status = create.status;
    let resData = create.data ? create.data : undefined;
    let message = "";

    switch (status) {
      case 201:
        message = "URL has been created";
        break;
      case 409:
        message = `URL with the name "${data.url_name}" with ${
          data.access_key ? `key "${data.access_key}"` : "no key"
        } is already created`;
        break;
      default:
        break;
    }

    //@ts-ignore
    res.status(status).json({ message: message, data: resData });
  }
}
