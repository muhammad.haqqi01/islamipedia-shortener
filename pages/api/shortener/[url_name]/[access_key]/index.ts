import { supabase } from "@/lib";
import { ResData } from "@/types";
import { NextApiRequest, NextApiResponse } from "next";
import { URL } from "@/types";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<ResData>
) {
  if (req.method === "GET") {
    const {
      url_name: url_name,
      access_key: access_key,
      getOnly: getOnly,
    } = req.query;

    const link = await supabase
      .from("url")
      .select()
      .match({ url_name: url_name, access_key: access_key })
      .single();
    const status = link.status;
    const data: URL = link.data as URL;
    let message = "";

    if (status === 200 && getOnly == "true") {
      res.status(status).json({
        message: "Get shortener URL data",
        data: { ...data, url_short: `${req.headers.host}/${url_name}/${access_key}` },
      });
    } else if (status === 200) {
      res.redirect(data.url);
    } //@ts-ignore
    else res.status(status).json({ message: message, data: data });
  }
}
