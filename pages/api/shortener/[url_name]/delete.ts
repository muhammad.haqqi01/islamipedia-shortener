import { supabase } from "@/lib";
import { ResData } from "@/types";
import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<ResData>
) {
  if (req.method === "DELETE") {
    const { url_name: url_name } = req.query;

    const link = await supabase
      .from("url")
      .delete()
      .match({ url_name: url_name, access_key: "" });
    const status = link.status;

    if (status === 204) {
      res.status(status).json({ message: "Delete success", data: null });
    } else res.status(status).json({ message: "No URL Found", data: null });
  }
}
