import quran from "@/quran.json";
import { supabase } from "@/lib";
import { ResData } from "@/types";
import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<ResData>
) {
  if (req.method === "GET") {
    const create = await supabase.from("quran").insert(quran);
    res.status(create.status).json({ message: "Done", data: null });
  }
}
