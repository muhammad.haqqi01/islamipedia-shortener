import quran from "@/quran.json";
import { supabase } from "@/lib";
import { ResData } from "@/types";
import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<ResData>
) {
  if (req.method === "GET") {
    const randomSurah = Math.floor(Math.random() * 113);
    const surah = await supabase
      .from("quran")
      .select()
      .match({ id: randomSurah + 1 })
      .single();

    const randomAyat = Math.floor(Math.random() * surah.data?.total_verses);
    if (surah.data) {
      surah.data.verses = [surah.data?.verses[randomAyat]];
    }

    res
      .status(surah.status)
      .json({ message: "Random surah & ayat", data: surah.data });
  }
}
