import { supabase } from "@/lib";
import { ResData } from "@/types";
import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<ResData>
) {
  if (req.method === "GET") {
    //@ts-ignore
    const { word }: { word: string } = req.query;
    const ayats = await supabase.from("quran").select();

    let occurence = 0;
    const result = ayats.data
      ?.map((i) => {
        return {
          ...i,
          verses: i.verses.filter((inner: any) => {
            return inner.translation
              .toLowerCase()
              .includes(
                ((" " + word.toLowerCase()) as string) ||
                  inner.translation
                    .toLowerCase()
                    .includes(word.toLowerCase() + " ")
              );
          }),
        };
      })
      .filter((i) => i.verses.length > 0)
      .map((i) => {
        i.verses.map((inner: any) => {
          occurence +=
            inner.translation.toLowerCase().split(word.toLowerCase()).length -
            1;
          return { ...inner };
        });
        return { ...i };
      });
    res.status(ayats.status).json({
      message: `There are ${occurence} appearances (note that quran is written in Arabic)`,
      data: result,
    });
  }
}
