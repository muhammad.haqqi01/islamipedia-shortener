import quran from "@/quran.json";
import { supabase } from "@/lib";
import { ResData } from "@/types";
import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<ResData>
) {
  if (req.method === "GET") {
    const { number, name, ayat } = req.query;
    let surah = undefined;

    if (number && name) {
      surah = await supabase
        .from("quran")
        .select()
        .match({ id: number, transliteration: name })
        .single();
    } else if (number) {
      surah = await supabase
        .from("quran")
        .select()
        .match({ id: number })
        .single();
    } else if (name) {
      surah = await supabase
        .from("quran")
        .select()
        .match({ transliteration: name })
        .single();
    } else {
      surah = await supabase.from("quran").select();
    }

    if (surah.data && (number || name) && ayat) {
      //@ts-ignore
      surah.data.verses = [surah.data.verses[parseInt(ayat) - 1]];
      //@ts-ignore
      if (surah.data.verses[0] === undefined) surah.data = undefined;
    }

    if (ayat && surah.data) {
      res
        .status(surah.status)
        .json({ message: "Found the surah & ayat", data: surah.data });
    } else if (surah && surah.data && !surah.error)
      res.status(surah.status).json({
        message: `Found the surah`,
        data: surah.data,
      });
    else
      res.status(surah.status).json({
        message: `Surah not found`,
        data: null,
      });
  }
}
