import quran from "@/quran.json";
import { supabase } from "@/lib";
import { ResData } from "@/types";
import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<ResData>
) {
  if (req.method === "GET") {
    const { word } = req.query;
    const ayats = await supabase.from("quran").select();

    let occurence = 0;
    const result = ayats.data
      ?.map((i) => {
        return {
          ...i,
          verses: i.verses.filter((inner: any) => {
            return inner.text.includes(word);
          }),
        };
      })
      .filter((i) => {
        return i.verses.length > 0;
      })
      .splice(0, 3)
      .map((i) => {
        i.verses.map((inner: any) => {
          occurence += inner.text.split(word).length - 1;
          return { ...inner };
        });
        return { ...i };
      });
    res.status(ayats.status).json({
      message: `There are ${occurence} appearances, note that sometimes it does not work`,
      data: result,
    });
  }
}
