import { sendQuotesAsync } from "../../../services/rabbit-mq/exchangeQuotes";

export default async function handler(req, res) {
  if (req.method === "POST") {
    if (
      req.body.ayat === "" ||
      req.body.email === "" ||
      (req.body.number === "" && req.body.name === "")
    ) {
      res.status(500).json({
        message: `There is an empty field`,
      });
    }
    const surah = await fetch(
      `http://${req.headers.host}/api/quran/surah?${new URLSearchParams(
        req.body
      )}`
    ).then((res) => res.json());

    if (!surah) {
      res.status(404).json({
        message: `Surah/ayat not found`,
      });
    }

    sendQuotesAsync({ ...surah, type: req.body.type, email: req.body.email });

    res.status(200).json({
      message: `Surah found! Sending the quotes soon to ${req.body.email}`,
    });
  }
}
