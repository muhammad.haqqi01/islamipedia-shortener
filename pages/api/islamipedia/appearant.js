export default async function handler(req, res) {
  if (req.method === "GET") {
    const { word } = req.query;

    const appearanceSurah = await fetch(
      `http://${req.headers.host}/api/quran/appearant-alphabet?word=${word}`
    ).then((res) => res.json());
    const appearanceHadith = await fetch(
      `http://${req.headers.host}/api/hadith/appearant?word=${word}`
    ).then((res) => res.json());

    res.status(200).json({
      message: `Found both appearance`,
      data: {
        hadith: appearanceHadith,
        quran: appearanceSurah,
      },
    });
  }
}
