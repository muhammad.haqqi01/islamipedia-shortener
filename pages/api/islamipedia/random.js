export default async function handler(req, res) {
  if (req.method === "GET") {
    const randomHadith = await fetch(
      `http://${req.headers.host}/api/hadith/random`
    )
      .then((res) => res.json())
      .then((data) => data.data);
    const randomQuran = await fetch(
      `http://${req.headers.host}/api/quran/random`
    )
      .then((res) => res.json())
      .then((data) => data.data);

    res.status(200).json({
      message: "Random hadith & ayat",
      data: {
        hadith: randomHadith,
        quran: randomQuran,
      },
    });
  }
}
