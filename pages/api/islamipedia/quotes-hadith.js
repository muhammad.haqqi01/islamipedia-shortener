import { sendQuotesAsync } from "../../../services/rabbit-mq/exchangeQuotes";

export default async function handler(req, res) {
  if (req.method === "POST") {
    if (
      req.body.number === "" ||
      req.body.email === "" ||
      req.body.book === ""
    ) {
      res.status(500).json({
        message: `There is an empty field`,
      });
    }

    const hadith = await fetch(
      `http://${req.headers.host}/api/hadith?${new URLSearchParams(req.body)}`
    ).then((res) => res.json());

    if (!hadith) {
      res.status(404).json({
        message: `Hadith not found`,
      });
    }

    sendQuotesAsync({ ...hadith, type: req.body.type, email: req.body.email });

    res.status(200).json({
      message: `Hadith found! Sending the quotes soon to ${req.body.email}`,
    });
  }
}
