import {
  Flex,
  FormLabel,
  Input,
  InputGroup,
  InputLeftAddon,
  Button,
  Box,
  Text,
  Link
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

function Shortener() {
  const [data, setData] = useState({
    url: "",
    url_name: "",
    access_key: "",
  });
  const [status, setStatus] = useState({
    show: false,
    type: "success",
    message: "",
    url: "",
  });
  const [loading, setLoading] = useState(true);
  const router = useRouter();

  async function handleAdd() {
    await fetch(`${window.location.origin}/api/shortener/create`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(
        data.access_key === ""
          ? {
              url: data.url,
              url_name: data.url_name,
            }
          : data
      ),
    })
      .then((res) => res.json())
      .then((data) => {
        if (!data.data) throw Error(data.message);
        setStatus({
          type: "success",
          show: true,
          message: data.message,
          url: data.data.access_key
            ? `${data.data.url_name}/${data.data.access_key}`
            : data.data.url_name,
        });
      })
      .catch((e) => {
        setStatus({ type: "error", show: true, message: e.message, url: "" });
      });
  }

  async function handleDelete() {
    await fetch(
      `${window.location.origin}/api/shortener/${data.url_name}/${data.access_key}/delete`,
      {
        method: "DELETE",
      }
    )
      .then(() => {
        setStatus({
          type: "success",
          show: true,
          message: "Deleted",
          url: "",
        });
      })
      .catch((e) => {
        setStatus({ type: "error", show: true, message: e.message, url: "" });
      });
  }

  useEffect(() => {
    setLoading(!router.isReady);
  }, [router.isReady]);

  return !loading ? (
    <Flex flexDirection="column" gap="4">
      <Box>
        <FormLabel>Original URL*</FormLabel>
        <Input
          isRequired
          onChange={(e) => setData({ ...data, url: e.target.value })}
        />
      </Box>
      <Flex gap="4">
        <Box w="70%">
          <FormLabel>URL Name*</FormLabel>
          <InputGroup>
            <InputLeftAddon children={`${window.location.origin}/`} />
            <Input
              isRequired
              onChange={(e) => setData({ ...data, url_name: e.target.value })}
            />
          </InputGroup>
        </Box>
        <Box w="30%">
          <FormLabel>Access Key</FormLabel>
          <Input
            onChange={(e) => setData({ ...data, access_key: e.target.value })}
          />
        </Box>
      </Flex>
      {!!router.asPath && status.show && status.type === "success" && status.url !== "" && (
        <Link
          target="_blank"
          href={`${window.location.origin}/api/shortener/${status.url}`}
        >
          <Text p="4" bg="green.100" textAlign="center" cursor="pointer">
            {window.location.origin}/api/shortener/{status.url}
          </Text>
        </Link>
      )}
      {status.show && status.type === "success" && status.url === "" && (
        <Text p="4" bg="green.100" textAlign="center">
          {status.message}
        </Text>
      )}
      {status.show && status.type === "error" && (
        <Text p="4" bg="red.300" textAlign="center">
          {status.message}
        </Text>
      )}
      <Flex gap="4">
        <Button colorScheme="blue" onClick={handleAdd} w="50%">
          Shorten URL
        </Button>
        <Button
          colorScheme="blue"
          variant="outline"
          onClick={handleDelete}
          w="50%"
        >
          Delete URL
        </Button>
      </Flex>
    </Flex>
  ) : (
    <div>loading</div>
  );
}

export default Shortener;
