import { Quran } from "@/types";
import { Flex, Text, Box } from "@chakra-ui/react";

export type verse = {
  id: number;
  text: string;
  translation: string;
};

function QuranCard({ transliteration, translation, id, verses }: Quran) {
  return (
    <Flex flexDirection="column" bg="gray.100" borderRadius="md" p="4">
      <Box>
        <Text
          fontWeight="bold"
          bg="gray.300"
          w="fit-content"
          px="4"
          py="2"
          borderRadius="md"
        >
          Surah {id} - {transliteration} ({translation})
        </Text>
      </Box>
      <Flex flexDirection="column" gap="4" m="2">
        {verses.map((i) => (
          <Flex flexDirection="column" gap="2">
            <Text w="fit-content">
              {/* @ts-ignore */}
              {i?.id as string}
            </Text>
            {/* @ts-ignore */}
            <Text textAlign="right">{i.text}</Text>
            {/* @ts-ignore */}
            <Text>{i?.translation as string}</Text>
          </Flex>
        ))}
      </Flex>
    </Flex>
  );
}

export default QuranCard;
