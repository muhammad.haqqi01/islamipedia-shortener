import { Hadith } from "@/types";
import { Flex, Box, Text } from "@chakra-ui/react";

type HadithCardType = Hadith & {
  isFirstAppearance: boolean;
};

function HadithCard({ id, number, book, isFirstAppearance }: HadithCardType) {
  return (
    <Flex flexDirection="column" bg="gray.100" borderRadius="md" p="4">
      {isFirstAppearance && (
        <Box>
          <Text
            fontWeight="bold"
            bg="gray.300"
            w="fit-content"
            px="4"
            py="2"
            borderRadius="md"
          >
            Hadith {book}
          </Text>
        </Box>
      )}
      <Flex flexDirection="column" gap="4" m="2">
        <Text>{number}</Text>
        <Text>{id}</Text>
      </Flex>
    </Flex>
  );
}

export default HadithCard;
