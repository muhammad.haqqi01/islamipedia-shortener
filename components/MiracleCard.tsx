import { Flex, Text } from "@chakra-ui/react";

type MiracleCardType = {
  text1: string;
  text2: string;
};

function MiracleCard({ text1, text2 }: MiracleCardType) {
  return (
    <Flex
      flexDirection="column"
      gap="2"
      bg="gray.100"
      p="4"
      borderRadius="md"
      w="full"
    >
      <Text>{text1}</Text>
      <Text>{text2}</Text>
    </Flex>
  );
}

export default MiracleCard;
