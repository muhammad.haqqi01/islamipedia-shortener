import { useState } from "react";
import {
  Flex,
  Input,
  Heading,
  Button,
  Text,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  Box,
  FormLabel,
  Image,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import QuranCard from "./QuranCard";
import { Hadith, Quran } from "@/types";
import HadithCard from "./HadithCard";
import MiracleCard from "./MiracleCard";

function Islamipedia() {
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [loading2, setLoading2] = useState(false);
  const [loading30, setLoading30] = useState(false);
  const [loading31, setLoading31] = useState(false);
  const [loading40, setLoading40] = useState(false);
  const [loading41, setLoading41] = useState(false);
  const [loading50, setLoading50] = useState(false);
  const [loading51, setLoading51] = useState(false);

  const [input, setInput] = useState("");
  const [input2, setInput2] = useState("");
  const [input30, setInput30] = useState({
    type: "A",
    number: "",
    name: "",
    ayat: "",
    email: "",
  });
  const [input31, setInput31] = useState({
    type: "A",
    book: "",
    number: "",
    email: "",
  });

  const [surahs, setSurahs] = useState([]);
  const [surahAppearances, setSurahAppearances] = useState("");
  const [hadiths, setHadiths] = useState([]);
  const [hadithAppearances, setHadithAppearances] = useState("");

  const [arabics, setArabics] = useState([]);
  const [arabicsAppearances, setArabicsAppearances] = useState("");

  const [surah, setSurah] = useState<Quran | Quran[]>({} as Quran);
  const [hadith, setHadith] = useState<Hadith | Hadith[]>({} as Hadith);

  const [surahRandom, setSurahRandom] = useState<Quran>({} as Quran);
  const [hadithRandom, setHadithRandom] = useState<Hadith>({} as Hadith);

  const [quotesSurahRes, setQuotesSurahRes] = useState<string>("");
  const [quotesHadithRes, setQuotesHadithRes] = useState<string>("");

  async function handleSearch() {
    function searchIslamipedia() {
      fetch(
        `${window.location.origin}/api/islamipedia/appearant?word=${input}`,
        {
          method: "GET",
        }
      )
        .then((res) => res.json())
        .then((data) => {
          console.log(data.data);
          setSurahs(data.data.quran.data);
          setSurahAppearances(data.data.quran.message);
          setHadiths(data.data.hadith.data);
          setHadithAppearances(data.data.hadith.message);
        })
        .then(() => setLoading(false));
    }
    setLoading(true);
    await searchIslamipedia();
  }

  async function handleSearchArabic() {
    setLoading2(true);
    fetch(
      `${window.location.origin}/api/quran/appearant-arabic?word=${input}`,
      {
        method: "GET",
      }
    )
      .then((res) => res.json())
      .then((data) => {
        setArabics(data.data);
        setArabicsAppearances(data.message);
      })
      .then(() => setLoading2(false));
  }

  async function handleSearchSingle(type: "quran/surah" | "hadith") {
    if (type === "quran/surah") setLoading30(true);
    else setLoading31(true);
    fetch(
      `${window.location.origin}/api/${type}?${
        type === "quran/surah"
          ? new URLSearchParams(input30)
          : new URLSearchParams(input31)
      }`,
      {
        method: "GET",
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (type === "quran/surah") setSurah(data.data);
        else setHadith(data.data);
      })
      .then(() => {
        if (type === "quran/surah") setLoading30(false);
        else setLoading31(false);
      });
  }

  async function handleSearchRandom(
    type: "quran/random" | "hadith/random" | "islamipedia/random"
  ) {
    if (type === "quran/random") setLoading40(true);
    else if (type === "hadith/random") setLoading41(true);
    else {
      setLoading40(true);
      setLoading41(true);
    }
    fetch(`${window.location.origin}/api/${type}`, {
      method: "GET",
    })
      .then((res) => res.json())
      .then((data) => {
        if (type === "quran/random") setSurahRandom(data.data);
        else if (type === "hadith/random") setHadithRandom(data.data);
        else {
          setSurahRandom(data.data.quran);
          setHadithRandom(data.data.hadith);
        }
      })
      .then(() => {
        if (type === "quran/random") setLoading40(false);
        else if (type === "hadith/random") setLoading41(false);
        else {
          setLoading40(false);
          setLoading41(false);
        }
      });
  }

  async function handleQuotes(type: "quran" | "hadith") {
    if (type === "quran") setQuotesSurahRes("Loading");
    else setQuotesHadithRes("Loading");
    fetch(`${window.location.origin}/api/islamipedia/quotes-${type}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(type === "quran" ? input30 : input31),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);
        if (type === "quran") setQuotesSurahRes(res.message);
        else setQuotesHadithRes(res.message);
      })
      .then(() => {
        if (type === "quran") setLoading50(false);
        else setLoading51(false);
      })
      .catch((e) => {
        if (type === "quran") {
          setQuotesSurahRes(e.message ? e.message : "Error unknown");
          setLoading50(false);
        } else {
          setQuotesHadithRes(e.message ? e.message : "Error unknown");
          setLoading51(false);
        }
      });
  }

  return (
    <Tabs variant="solid-rounded">
      <TabList w="100%" textAlign="center">
        <Tab w="full">Find Appearance</Tab>
        <Tab w="full">Arabic Quran Appearance</Tab>
        <Tab w="full">Quran & Hadith Finder</Tab>
        <Tab w="full">Quran & Hadith Randomizer</Tab>
        <Tab w="full">Quran & Hadith Quotes</Tab>
      </TabList>

      <TabPanels>
        <TabPanel>
          <Flex flexDirection="column" gap="8">
            <Heading fontSize="2xl" textAlign="center" mt="8">
              Search words from the Al Qur'an & Hadith (Indonesian alphabet)
            </Heading>
            <Text textAlign="center">
              Note : Since the Al-Qur'an is written in Arabic, it is cannot
              fully proof word appearances miracle
            </Text>
            <Flex flexDirection="column" gap="4">
              <Text>
                One of the my favourite miracle is the number of some arabic
                word occurence in the Quran
              </Text>
              <Flex gap="4">
                <MiracleCard text1="Adam: 25" text2="Isa: 25" />
                <MiracleCard text1="Day: 365" text2="Month: 12" />
                <MiracleCard text1="Dunya: 115" text2="Akhirat: 115" />
                <MiracleCard text1="Angels: 25" text2="Devils: 25" />
                <MiracleCard text1="Say: 332" text2="They said: 332" />
                <MiracleCard text1="Contradiction: 1" text2="" />
              </Flex>
              <Text>
                Note in 1: In Ali 'Imran:59, Allah said "Sesungguhnya
                perumpamaan Isa bagi Allah seperti Adam" so Allah appeared their
                name in the same number too. Also from Al-Fatihah to Ali
                'Imran:59, both Adam & Isa appear 7 times.
              </Text>
              <Text>
                Note in 6: In An-Nisa:82, Allah said "Maka tidakkah mereka
                menghayati (mendalami) Alquran? Sekiranya (Alquran) itu bukan
                dari Allah, pastilah mereka menemukan banyak hal yang
                bertentangan di dalamnya" so Allah appear the word with the
                meaning "Contradiction/Bertentangan" only 1 time since there is
                not "Banyak hal yang bertentangan" (not more than 1, so it's
                "not much/tidak banyak")
              </Text>
            </Flex>
            <Flex gap="4">
              <Input
                placeholder="minuman keras / ramadhan / nikah"
                onChange={(e) => setInput(e.target.value)}
              />
              <Button colorScheme="blue" onClick={handleSearch}>
                Cari
              </Button>
            </Flex>
            {!loading ? (
              <Flex flexDirection="column" gap="4">
                <Heading fontSize="xl">Quran</Heading>
                <Text>{surahAppearances}</Text>
                {surahs.map((i: Quran) => (
                  <QuranCard {...i} />
                ))}

                <Heading fontSize="xl" mt="8">
                  Hadith
                </Heading>
                <Text>{hadithAppearances}</Text>
                {hadiths.map((i: Hadith, index: number) => (
                  <HadithCard
                    {...i}
                    isFirstAppearance={
                      index === 0 ||
                      // @ts-ignore
                      hadiths[index - 1].book !== hadiths[index].book
                    }
                  />
                ))}
              </Flex>
            ) : (
              <Text textAlign="center">Loading</Text>
            )}
          </Flex>
        </TabPanel>
        <TabPanel>
          <Flex flexDirection="column" gap="8">
            <Heading fontSize="2xl" textAlign="center" mt="8">
              Search word from the Al Qur'an in Arabic Letters
            </Heading>
            <Text textAlign="center">
              Note : Sometimes the search does not work since Arabic word has
              many ways to write
            </Text>
            <Flex gap="4">
              <Input
                placeholder="عِيسَى"
                onChange={(e) => setInput(e.target.value)}
              />
              <Button colorScheme="blue" onClick={handleSearchArabic}>
                Cari
              </Button>
            </Flex>
            {!loading2 ? (
              <Flex flexDirection="column" gap="4">
                <Heading fontSize="xl">Quran (Arabic)</Heading>
                <Text>{arabicsAppearances}</Text>
                {arabics.map((i: Quran) => (
                  <QuranCard {...i} />
                ))}
              </Flex>
            ) : (
              <Text textAlign="center">Loading</Text>
            )}
          </Flex>
        </TabPanel>
        <TabPanel>
          <Flex flexDirection="column" gap="8">
            <Box>
              <Heading fontSize="2xl" textAlign="center" mt="8">
                Search from the Al Qur'an & Hadith (Indonesian)
              </Heading>
              <Text textAlign="center" mt="8">
                Fill either nomor surah or nama surah (spesifik)
              </Text>
            </Box>
            <Flex gap="4" flexDirection="column">
              <Flex gap="4" w="full">
                <Box w="full">
                  <FormLabel>Surah keberapa*</FormLabel>
                  <Input
                    placeholder="114"
                    onChange={(e) =>
                      setInput30({ ...input30, number: e.target.value })
                    }
                  />
                </Box>
                <Box w="full">
                  <FormLabel>Nama Surah*</FormLabel>
                  <Input
                    placeholder="An-Nas"
                    onChange={(e) =>
                      setInput30({ ...input30, name: e.target.value })
                    }
                  />
                </Box>
                <Box w="full">
                  <FormLabel>Ayat keberapa</FormLabel>
                  <Input
                    placeholder="286"
                    onChange={(e) =>
                      setInput30({ ...input30, ayat: e.target.value })
                    }
                  />
                </Box>
              </Flex>
              <Button
                colorScheme="blue"
                onClick={() => handleSearchSingle("quran/surah")}
              >
                Cari
              </Button>
            </Flex>
            <Flex gap="4" flexDirection="column">
              <Flex gap="4" w="full">
                <Box w="full">
                  <FormLabel>Buku apa*</FormLabel>
                  <Input
                    placeholder="bukhari"
                    onChange={(e) =>
                      setInput31({ ...input31, book: e.target.value })
                    }
                  />
                </Box>
                <Box w="full">
                  <FormLabel>Nomor Hadith</FormLabel>
                  <Input
                    placeholder="2001"
                    onChange={(e) =>
                      setInput31({ ...input31, number: e.target.value })
                    }
                  />
                </Box>
              </Flex>
              <Button
                colorScheme="blue"
                onClick={() => handleSearchSingle("hadith")}
              >
                Cari
              </Button>
            </Flex>
            {!loading30 ? (
              <Flex flexDirection="column" gap="4">
                <Heading fontSize="xl">Quran</Heading>
                {/* @ts-ignore */}
                {surah && surah.verses && <QuranCard {...surah} />}

                {surah &&
                  /* @ts-ignore */
                  surah.length > 0 &&
                  //@ts-ignore
                  surah.map((i: Quran) => <QuranCard {...i} />)}
                {!surah && <Text>Surah not found</Text>}
              </Flex>
            ) : (
              <Text textAlign="center">Loading</Text>
            )}
            {!loading31 ? (
              <Flex flexDirection="column" gap="4">
                <Heading fontSize="xl">Hadith</Heading>
                {/* @ts-ignore */}
                {hadith && hadith.book && (
                  //@ts-ignore
                  <HadithCard {...hadith} isFirstAppearance={true} />
                )}

                {hadith &&
                  //@ts-ignore
                  hadith.length > 0 &&
                  //@ts-ignore
                  hadith.map((i: Hadith, index: number) => (
                    <HadithCard {...i} isFirstAppearance={index === 0} />
                  ))}
                {!hadith && <Text>Hadith not found</Text>}
              </Flex>
            ) : (
              <Text textAlign="center">Loading</Text>
            )}
          </Flex>
        </TabPanel>
        <TabPanel>
          <Flex flexDirection="column" gap="8">
            <Heading fontSize="2xl" textAlign="center" mt="8">
              Search random Al Qur'an Verse & Hadith (Indonesian)
            </Heading>

            <Flex gap="4">
              <Button
                w="50%"
                colorScheme="blue"
                onClick={() => handleSearchRandom("quran/random")}
              >
                Cari Ayat
              </Button>

              <Button
                w="50%"
                colorScheme="blue"
                onClick={() => handleSearchRandom("hadith/random")}
              >
                Cari Hadith
              </Button>
            </Flex>
            <Button
              w="100%"
              colorScheme="blue"
              onClick={() => handleSearchRandom("islamipedia/random")}
            >
              Cari Keduanya
            </Button>

            {!loading40 ? (
              <Flex flexDirection="column" gap="4">
                <Heading fontSize="xl">Quran</Heading>
                {/* @ts-ignore */}
                {surahRandom && surahRandom.verses && (
                  <QuranCard {...surahRandom} />
                )}
              </Flex>
            ) : (
              <Text textAlign="center">Loading</Text>
            )}
            {!loading41 ? (
              <Flex flexDirection="column" gap="4">
                <Heading fontSize="xl">Hadith</Heading>
                {/* @ts-ignore */}
                {hadithRandom && hadithRandom.book && (
                  //@ts-ignore
                  <HadithCard {...hadithRandom} isFirstAppearance={true} />
                )}
              </Flex>
            ) : (
              <Text textAlign="center">Loading</Text>
            )}
          </Flex>
        </TabPanel>
        <TabPanel>
          <Flex flexDirection="column" gap="8">
            <Flex flexDirection="column" gap="8">
              <Box>
                <Heading fontSize="2xl" textAlign="center" mt="8">
                  Create Al Qur'an/Hadith Quotes
                </Heading>
                <Text textAlign="center" mt="8">
                  Fill either nomor surah or nama surah (specific & alphabet)
                  for quran
                </Text>
              </Box>
              <Flex
                gap="4"
                justifyContent="space-evenly"
                fontWeight="bold"
                flexWrap="wrap"
              >
                <Box
                  flexBasis="45%"
                  borderRadius="md"
                  bg={input30.type === "A" ? "gray.100" : "inherit"}
                  p="4"
                  textAlign="center"
                  cursor="pointer"
                  onClick={() => {
                    setInput30({ ...input30, type: "A" });
                    setInput31({ ...input31, type: "A" });
                  }}
                >
                  <Image src="./A_sample.png" objectFit="cover" />
                  <Text mt="4">Model A</Text>
                </Box>
                <Box
                  flexBasis="45%"
                  borderRadius="md"
                  bg={input30.type === "B" ? "gray.100" : "inherit"}
                  p="4"
                  textAlign="center"
                  cursor="pointer"
                  onClick={() => {
                    setInput30({ ...input30, type: "B" });
                    setInput31({ ...input31, type: "B" });
                  }}
                >
                  <Image src="./B_sample.png" objectFit="cover" />
                  <Text mt="4">Model B</Text>
                </Box>
                <Box
                  flexBasis="45%"
                  borderRadius="md"
                  bg={input30.type === "C" ? "gray.100" : "inherit"}
                  p="4"
                  textAlign="center"
                  cursor="pointer"
                  onClick={() => {
                    setInput30({ ...input30, type: "C" });
                    setInput31({ ...input31, type: "C" });
                  }}
                >
                  <Image src="./C_sample.png" objectFit="cover" />
                  <Text mt="4">Model C</Text>
                </Box>
                <Box
                  flexBasis="45%"
                  borderRadius="md"
                  bg={input30.type === "D" ? "gray.100" : "inherit"}
                  p="4"
                  textAlign="center"
                  cursor="pointer"
                  onClick={() => {
                    setInput30({ ...input30, type: "D" });
                    setInput31({ ...input31, type: "D" });
                  }}
                >
                  <Image src="./D_sample.png" objectFit="cover" />
                  <Text mt="4">Model D</Text>
                </Box>
              </Flex>
              <Flex gap="4" flexDirection="column">
                <Box w="full">
                  <FormLabel>Email pengiriman*</FormLabel>
                  <Input
                    type="email"
                    placeholder="abc@example.com"
                    onChange={(e) =>
                      setInput30({ ...input30, email: e.target.value })
                    }
                  />
                </Box>
                <Flex gap="4" w="full">
                  <Box w="full">
                    <FormLabel>Surah keberapa*</FormLabel>
                    <Input
                      placeholder="114"
                      onChange={(e) =>
                        setInput30({ ...input30, number: e.target.value })
                      }
                    />
                  </Box>
                  <Box w="full">
                    <FormLabel>Nama Surah*</FormLabel>
                    <Input
                      placeholder="An-Nas"
                      onChange={(e) =>
                        setInput30({ ...input30, name: e.target.value })
                      }
                    />
                  </Box>
                  <Box w="full">
                    <FormLabel>Ayat keberapa*</FormLabel>
                    <Input
                      isRequired
                      placeholder="286"
                      onChange={(e) =>
                        setInput30({ ...input30, ayat: e.target.value })
                      }
                    />
                  </Box>
                </Flex>
                <Button
                  colorScheme="blue"
                  onClick={() => handleQuotes("quran")}
                >
                  Cari
                </Button>
              </Flex>
              <Flex gap="4" flexDirection="column">
                <Box w="full">
                  <FormLabel>Email pengiriman*</FormLabel>
                  <Input
                    type="email"
                    placeholder="abc@example.com"
                    onChange={(e) =>
                      setInput31({ ...input31, email: e.target.value })
                    }
                  />
                </Box>
                <Flex gap="4" w="full">
                  <Box w="full">
                    <FormLabel>Buku apa*</FormLabel>
                    <Input
                      placeholder="bukhari"
                      onChange={(e) =>
                        setInput31({ ...input31, book: e.target.value })
                      }
                    />
                  </Box>
                  <Box w="full">
                    <FormLabel>Nomor Hadith*</FormLabel>
                    <Input
                      isRequired
                      placeholder="2001"
                      onChange={(e) =>
                        setInput31({ ...input31, number: e.target.value })
                      }
                    />
                  </Box>
                </Flex>
                <Button
                  colorScheme="blue"
                  onClick={() => handleQuotes("hadith")}
                >
                  Cari
                </Button>
              </Flex>
              {!loading50 ? (
                <Flex flexDirection="column" gap="4">
                  <Text>{quotesSurahRes}</Text>
                </Flex>
              ) : (
                <Text textAlign="center">Loading</Text>
              )}
              {!loading51 ? (
                <Flex flexDirection="column" gap="4">
                  <Text>{quotesHadithRes}</Text>
                </Flex>
              ) : (
                <Text textAlign="center">Loading</Text>
              )}
            </Flex>
          </Flex>
        </TabPanel>
      </TabPanels>
    </Tabs>
  );
}

export default Islamipedia;
