# Islamipedia & Shortener
File README.MD yang lebih rapi dapat diakses [disini](https://gitlab.com/muhammad.haqqi01/islamipedia-shortener/-/blob/main/README.md?plain=0)

[Link Repo](https://gitlab.com/muhammad.haqqi01/islamipedia-shortener)

Deployment terdapat di [http://35.209.104.21:3000](http://35.209.104.21:3000) atau [http://localhost:3000](http://localhost:3000) apabila ingin mencoba pada local

Cara menyalakan :
- jalankan rabbitmq terlebih dahulu 
- npm install (apabila baru pertama kali menjalankan)
- cd islamipedia-shortener
- npm run dev


<details>
<summary>Tugas Mandiri 2</summary>

# Islamipedia new feature: Quotes Maker
Membuat quotes berdasarkan isi Al-Quran & Hadist secara mudah dan otomatis hanya dengan memilih template & beberapa input saja. Proses yang dilakukan menjadi sangat cepat karena bersifat asinkronus sehingga hanya dalam beberapa detik saja, proses bisa ditinggal lalu hasilnya akan dikirim pada email yang dituju.

> Saya berfokus pada tambahan fitur yang dibuat untuk tugas mandiri 2 pada bagian ini, untuk seluruh fitur lain terdapat pada section tugas mandiri 1

# Informasi dasar
## Mengenai repo
- Bahasa : JavaScript + TypeScript
- Framework : Next.js
- Packages : ChakraUI, RabbitMQ, puppeteer, nodemailer, logger
- Database : Supabase
- Deployment : npm run dev (tugas 2), Google Cloud

## API yang digunakan

- Islamipedia API : **/api/islamipedia** <br>

## Data yang digunakan

- Qur'an : https://github.com/risan/quran-json (json) (114 column berisi seluruh surah, total ayat sebanyak 6236) <br>
- Hadith : https://github.com/gadingnst/hadith-api  (json) (column berisi 9 buku dengan total 38102 nomor buku)

## Alasan tidak ada menggunakan public API

- Banyak fungsi yang sangat tidak efisien dijalankan apabila menggunakan public API (contoh: tidak ada satupun API yang mengembalikan seluruh surah dengan ayat sehingga harus melakukan GET sebanyak 114 kali untuk dapat surah)
- Banyak public API yang sangat lambat untuk didapatkan valuenya, belum ditambah saat diproses (contoh: hadits API harus mengecek 30000 data)
- Beberapa public API ada yang harus membayar/memberi kredit untuk menggunakannya
- Menambah challenge
- Saya tidak sengaja telah membuat seluruh API sendiri


# Dokumentasi API

Base URL = **"http://35.209.104.21:3000/api"** | **"http://localhost:3000/api"** (local)

## Islamipedia API

Main endpoint = **"/islamipedia"** <br>
List :
- **/quotes-quran** (POST)<br>
Create quotes from selected quran surah & ayat then send it to email<br>
Body: 
    - type (A | B | C | D ): template selected, 
    - email (string): target email to send the quotes, 
    - ayat (number): ayat number, 
    - id (number): surah number, 
    - name (string): surah name<br>
- **/quotes-hadith** (POST)<br>
Create quotes from selected hadith book & number then send it to email<br>
Body: 
    - type (A | B | C | D): template selected,
    - email (string): target email to send the quotes,
    - book (string): hadith author,
    - number (number): book number<br>

# Tingkat kesulitan

## General + Deployment
- Tidak ada dokumentasi yang memberikan contoh penggunaan Next.js backend dengan RabbitMQ. Contoh terdekatnya adalah dengan Node.js
- Pertama kali menggunakan RabbitMQ
- Membuat exchange sangatlah sulit sehingga saya hanya menggunakan Queue biasa
- Mencari template background yang gratis & layouting yang bagus
- Adanya masalah environment pada GCP & docker seperti versi image node, puppeteer yang membutuhkan chromium, adjustment GCP & docker karena RabbitMQ.
- Butuh penginstalan RabbitMQ pada ubuntu & docker 

## Islamipedia API
- Membutuhkan kepastian bahwa Hadith API & Al-Quran API berjalan dengan benar
- Membuat RabbitMQ dapat mengconsume isi queue selama terisi tanpa memanggil dengan endpoint.
- Mengadjust text dan styling secara manual (hardcode) & beragam bergantung pada template dengan kode untuk membuat quotes
- Membutuhkan berbagai library (puppeteer, nodemailer, RabbitMQ) hanya untuk membuat 1 endpoint
- Memastikan bahwa pada endpoint hanya sekali memasukkan ke queue sehingga proses tidak memerlukan await
- Membuat queue yang dapat dengan cepat memproses data tergantung endpoint sekaligus memastikan gambarnya tidak overlap dengan proses lain.
- Susah untuk memastikan bahwa seluruh kode pada runner sudah berjalan karena adanya sendToQueue pada queue yang tidak bisa dicek dengan console.log() sehingga dibutuhkan library logger

## Tampilan Frontend
- Memberi desain yang nyaman & mudah digunakan dengan peletakan posisi elemen & warna yang tepat
- Tampilan yang interaktif dengan merepresentasikan jalannya API
- Pada awal deployment, terdapat beragam masalah pada tampilan seperti form yang tidak bekerja, gambar yang tidak bisa diload.

# Use Case & Impact

## Islamipedia API
- Memberikan tampilan yang menarik untuk melihat suatu ayat atau isi buku hadist
- Dapat membuat gambar quotes dengan ayat al quran atau isi hadist secara otomatis
- Otomasisasi dapat membuat kemungkinan banyaknya pengguna dikarenakan lebih cepat & sederhana
- Harapannya dapat membuat orang lain menjadi lebih tertarik untuk membaca ayat atau isi hadist dengan bentuk quotes
- Dapat berpotensi untuk dikembangkan lebih jauh seperti dengan menambah custom background atau background bawaan lainnya

## Tampilan FE
- Dapat mencoba API secara langsung dengan tampilan interaktif
- Tidak perlu mengimplementasikan API untuk bisa menggunakan fitur quotes

# Keunikan
Sejauh ini saya belum menemukan service seperti yang saya buat dikarenakan saya menggabungkan Al-Quran & Hadist dengan automasi gambar untuk membuat quotes. Service terdekat yang mirip dengan saya hanya saya temukan 1, yaitu yang membuat quotes otomatis tetapi bentuknya masih sederhana dan belum menerapkan asynchronous web service.

</details>

<details>
<summary>Tugas Mandiri 1</summary>
<br>

# Informasi dasar
## Mengenai repo
- Bahasa : JavaScript + TypeScript
- Framework : Next.js
- Packages : ChakraUI
- Database : Supabase
- Deployment : Docker, Google Cloud

## API yang digunakan

- Islamipedia API : **/api/islamipedia** <br>
- Shortener API : **/api/shortener** <br>
- Custom-made Al-Qur'an API : **/api/quran** <br>
- Custom-made Hadith API : **/api/hadith** <br>


## Data yang digunakan

- Qur'an : https://github.com/risan/quran-json (json) (114 column berisi seluruh surah, total ayat sebanyak 6236) <br>
- Hadith : https://github.com/gadingnst/hadith-api  (json) (column berisi 9 buku dengan total 38102 nomor buku)

## Alasan tidak ada menggunakan public API

- Banyak fungsi yang sangat tidak efisien dijalankan apabila menggunakan public API (contoh: tidak ada satupun API yang mengembalikan seluruh surah dengan ayat sehingga harus melakukan GET sebanyak 114 kali untuk dapat surah)
- Banyak public API yang sangat lambat untuk didapatkan valuenya, belum ditambah saat diproses (contoh: hadits API harus mengecek 30000 data)
- Beberapa public API ada yang harus membayar/memberi kredit untuk menggunakannya
- Menambah challenge
- Saya tidak sengaja telah membuat seluruh API sendiri


# Dokumentasi API

Base URL = **"http://35.209.104.21:3000/api"**

## Islamipedia API

Main endpoint = **"/islamipedia"** <br>
List :
- **/random** (GET)<br>
Get random Al-Quran ayat & Hadith<br>
- **/appearant** (GET)<br>
Get indonesian translated word appearance in Al-Qur'an & Hadith<br>
Query: {word}: word to be found

## Al-Qur'an API

Main endpoint = **"/quran"** <br>
List :
- **/random** (GET)<br>
Get random surah from Al-Qur'an
- **/surah** (GET) <br>
Get surah from Al-Quran<br>
Query: {ayat}: ayat number, {id}: surah number, {name}: surah name<br>
- **/appearant-arabic** (GET)<br>
Get arabic word appearance in Al-Qur'an (not fully functional)<br>
- **/appearant-alphabet** (GET)<br>
Get indonesian translated word appearance in Al-Qur'an

## Hadith API

Main endpoint = **"/hadith"** <br>
List :
- **/random** (GET)<br>
Get random hadith from
- **/** (GET) <br>
Get surah from Al-Quran<br>
Query: {book}: hadith author, {number}: book number<br>
- **/appearant** (GET)<br>
Get indonesian translated word appearance in Al-Qur'an

## Shortener API

Main endpoint = **"/shortener"** <br>
List :
- **/{name}** (GET)<br>
Jump to url with url_name = {name} <br>
Query: {getOnly}: Get data only, no redirect
- **/{name}/{access_key}/** (GET) <br>
Jump to url with {access_key} with url_name = {name}<br>
Query: {getOnly}: Get data only, no redirect<br>
- **/create** (POST)<br>
Create a new shortener url <br>
body :<br>

    url: string => URL to be shorten,<br>
    url_name: string => shorten URL name, <br>
    access_key: string (optional) => key to open the URL <br>
- **/{name}/delete** (DELETE) <br>
Delete shorten url with url_name = name<br>
- **/{name}/{access_key}/delete** (DELETE) <br>
Delete url with {access_key} and with url_name = {name} <br>
<br>

# Tingkat kesulitan

## Islamipedia API
- Membutuhkan kepastian bahwa Hadith API & Al-Quran API berjalan dengan benar

## Hadith API
- Mencari data awal yang lengkap dan detail mengenai hadits cukup sulit
- Input query user yang beragam

## Al-Quran API
- Mencari seluruh kata yang muncul pada Al-Quran cukup sulit melihat banyak case kemunculan kata.
- Mencari kata arabic memiliki bentuk penulisan yang beragam (gundul, berharakat, etc).
- Mencari data awal yang lengkap dan detail mengenai al-quran cukup sulit
- Input query user yang beragam
- Penggunaan TypeScript

## Shortener API
- Memastikan bahwa url berisi url yang valid
- Butuh memastikan apakah url yang ingin dibuat sudah ada pada database
- Membuat performance yang cepat saat menembak url yang telah dibuat
- Penggunaan TypeScript

## Tampilan Frontend
- Memberi desain yang nyaman & mudah digunakan dengan peletakan posisi elemen & warna yang tepat
- Tampilan yang interaktif dengan merepresentasikan sebanyak mungkin endpoint API tanpa berpindah halaman dengan rapi cukup susah & kompleks untuk dibuat

<br>

# Use Case & Impact

## Islamipedia API
- Mendapatkan random hadits & quran apabila ingin mengeksplor keduanya tetapi tidak tahu mau yang mana
- Mendapat informasi lengkap dari referensi langsung ke kedua sumber mengenai suatu topik
- Menambah wawasan agama
- Mencari kebenaran dari suatu hal dari hadist & quran

## Hadith API
- Mencari hadits yang diinginkan dengan mudah
- Ingin mencoba untuk explore berbagai macam hadits secara random
- Mencari hadits yang membahas hal khusus dengan mencari kata pada hadits
- Menambah wawasan agama dari hadits

## Al-Quran API
- Membuktikan beberapa mukjizat mengenai jumlah kata yang muncul pada Al-Qur'an dari sisi translasi maupun arabic
- Mencari pembahasan mengenai hal khusus pada Al-Qur'an dengan mencari kata pada Al-Qur'an
- Mencari surah dan ayat yang diingingkan dengan mudah
- Mencoba untuk explore berbagai macam ayat secara random
- Menambah wawasan agama dari Al-Quran

## Shortener API
- Memendekkan URL sehingga lebih mudah diingat
- Memberi nama pada URL tertentu sehingga lebih mudah diakses

## Tampilan FE
- Dapat mencoba API secara langsung dengan tampilan interaktif
- Tidak perlu mengimplementasikan API untuk bisa menggunakan hampir keseluruhan case yang saya buat
- Mendapatkan wawasan mengenai Al-Quran & Hadis sekaligus bisa melakukan shorten url dari implementasi tampilannya langsung
</details>
