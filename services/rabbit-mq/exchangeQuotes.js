import amqp from "amqplib";
import QuotesImageGenerator from "../QuotesImageGenerator";
import { sendEmailAsync } from "./exchangeEmail";
import { logger } from "./logger";

let connection = "";
let channel = "";

// RabbitMQ connection
export async function connectToRabbitMQQuotes() {
  connection = await amqp.connect("amqp://localhost");
  logger.info("Connected to RabbitMQ Quotes Server");
  channel = await connection.createChannel();
  logger.info("Created RabbitMQ Quotes Channel");
  await channel.assertQueue("quotesQuran", { durable: true });
  await channel.assertQueue("quotesHadith", { durable: true });
  logger.info("Initialized RabbitMQ Quotes Queues");
  return { channel };
}

export async function sendQuotesAsync(message) {
  // const { channel } = exchangesContext();
  logger.info(`sent: ${message} to queue quotes`);
  channel.sendToQueue(
    message.data.verses ? "quotesQuran" : "quotesHadith",
    Buffer.from(JSON.stringify(message)),
    {
      persistent: true,
    }
  );
}

connectToRabbitMQQuotes().then(({ channel }) => {
  channel.consume(
    "quotesQuran",
    function (msg) {
      logger.info("Inside the queue quotes quran");

      let outerData = JSON.parse(msg.content);
      //prevent the latter file being overwritten when sending email
      setTimeout(async () => {
        const generator = new QuotesImageGenerator();
        await generator.start({
          type: outerData.type,
          quote: `"${outerData.data.verses[0].translation}"`,
          author: `Surah ${outerData.data.transliteration} - ${outerData.data.verses[0].id}`,
          path: "./my-quote-quran.png",
          backgroundColorActivate: false,
          blur: 0,
        });

        sendEmailAsync(outerData.email, "quran");
      }, 5000);
    },
    {
      noAck: true,
    }
  );

  channel.consume(
    "quotesHadith",
    async function (msg) {
      logger.info("Inside the queue quotes hadith");

      let outerData = JSON.parse(msg.content);
      //prevent the latter file being overwritten when sending email
      setTimeout(async () => {
        const generator = new QuotesImageGenerator();
        await generator.start({
          type: outerData.type,
          quote: `"${outerData.data.id}"`,
          author: `Hadith ${outerData.data.book} - ${outerData.data.number}`,
          path: "./my-quote-hadith.png",
          backgroundColorActivate: false,
          blur: 0,
        });

        sendEmailAsync(outerData.email, "hadith");
      }, 5000);
    },
    {
      noAck: true,
    }
  );
});
