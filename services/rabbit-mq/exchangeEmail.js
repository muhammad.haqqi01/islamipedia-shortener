import amqp from "amqplib";
import sendEmail from "../SendEmail";
import { logger } from "./logger";

let connection = "";
let channel = "";

// RabbitMQ connection
export async function connectToRabbitMQEmail() {
  connection = await amqp.connect("amqp://localhost");
  logger.info("Connected to RabbitMQ Email Server");
  channel = await connection.createChannel();
  logger.info("Created RabbitMQ Email Channel");
  await channel.assertQueue("email", { durable: true });
  logger.info("Initialized RabbitMQ Email Queues");
  return { connection, channel };
}

export async function sendEmailAsync(email, type) {
  logger.info(`sent: ${email} to queue email`);
  channel.sendToQueue(
    "email",
    Buffer.from(JSON.stringify({ email: email, type: type })),
    {
      persistent: true,
    }
  );
}

connectToRabbitMQEmail().then(({ channel }) => {
  channel.consume(
    "email",
    async function (msg) {
      logger.info("Inside the queue email");
      let message = JSON.parse(msg.content);
      await sendEmail(message.email, message.type);
    },
    {
      noAck: true,
    }
  );
});
