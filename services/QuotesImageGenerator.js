import fs from "fs";
import pathM from "path";
import puppeteer from "puppeteer";

export default class QuotesPhotosGenerator {
  start({
    type,
    quote,
    specialWords = [],
    author = "",
    path = "./image.png",
    blur = 0,
    backgroundColorActivate = true,
    width = 1280,
    height = 720,
  }) {
    const { color, font } = filterType(type);

    return new Promise(async (next) => {
      const htmlCode = this.createHTMLCode({
        type,
        color,
        quote,
        specialWords,
        font,
        author,
        backgroundColorActivate,
        blur,
      });
      this.writeHTMLCode({ htmlCode, path });
      await this.saveImage({ path, height, width });
      this.removeHTMLCode({ path });
      next();
    });
  }

  createHTMLCode({
    type,
    color,
    quote,
    specialWords,
    font,
    author,
    blur,
    backgroundColorActivate,
  }) {
    const quoteSpanHTML = this.createQuoteSpanHTML({ quote, specialWords });

    const html = `
      <!DOCTYPE html>
      <html>
        <head>
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <style>
            @import url('https://fonts.googleapis.com/css2?family=Alata&family=Bellefair&family=Playfair+Display:ital,wght@1,500&family=Poppins:wght@400;500&display=swap');            
            html {
              height: 100%;
            }
            body, .image {
              width: 100%;
              height: 100%;
              margin: 0;
            }
            .image {
              background-image: url(../../../public/${type}.png);
              background-size: cover;
              background-position: center;
              filter: blur(${blur}px);
            }
            .colorShadow, .darkShadow {
              width: 100%;
              height: 100%;
              display: flex;
              justify-content: center;
              align-items: center;
            }
            .colorShadow {
              background-color: ${
                backgroundColorActivate ? `#${color}40` : `transparent`
              };
              position: absolute;
              top: 0;
            }
            
            .content {
              padding: 12px;
              margin: 16px;
              border: 6px solid white;
              width: calc(100% - 68px);
              height: calc(100% - 68px);
              display: flex;
              align-items: center;
              justify-content: center;
              position: relative;
              font-weight: ${font.weight};
              color: #${color};
            }
            .content span {
              font-family: ${font.family};
              font-size: 24px;
              text-align: center;
              font-style: ${font.style}
            }
            .content span .specialWord {
              color: #${color};
            }
            .content .author {
              color: #${color};
              font-family: ${font.family};
              font-size: 20px;
              position: absolute;
              bottom: 8px;
              width: 100%;
              filter: brightness(0.8);
              text-align: center;
            }
          </style>
        </head>
        <body>
          <div class="image"></div>
          <div class="colorShadow">
            <div class="darkShadow">
              <div class="content">
                <span>${quoteSpanHTML}</span>
                <div class="author">
                  ${author}
                </div>
              </div>
            </div>
          </div>
        </body>
      </html>
    `;

    return html;
  }

  writeHTMLCode({ htmlCode, path }) {
    fs.writeFileSync(
      `${pathM.join(__dirname, `../../${path}`)}.html`,
      htmlCode
    );
  }

  async saveImage({ path, width, height }) {
    const browser = await puppeteer.launch({
      args: [`--window-size=${width},${height}`],
    });
    const page = await browser.newPage();
    await page.setViewport({ width, height });
    await page.goto(`file://${pathM.join(__dirname, `../../${path}`)}.html`);
    await page.screenshot({ path });
    await browser.close();
  }

  removeHTMLCode({ path }) {
    fs.unlinkSync(`${pathM.join(__dirname, `../../${path}`)}.html`);
  }

  createQuoteSpanHTML({ quote, specialWords }) {
    let quoteSpanHTML = quote;
    specialWords.forEach(
      (specialWord) =>
        (quoteSpanHTML = quoteSpanHTML.replaceAll(
          specialWord,
          `<span class="specialWord">${specialWord}</span>`
        ))
    );
    return quoteSpanHTML;
  }
}

String.prototype.replaceAll = function (search, replacement) {
  return this.split(search).join(replacement);
};

function filterType(type) {
  if (type === "A") {
    return {
      color: "1C1468",
      font: {
        family: "'Playfair Display', serif",
        style: "italic",
        weight: "bold",
      },
    };
  } else if (type === "B") {
    return {
      color: "7C7676",
      font: {
        family: "'Bellefair', serif",
        style: "normal",
        weight: "thin",
      },
    };
  } else if (type === "C") {
    return {
      color: "FFFFFF",
      font: {
        family: "'Poppins', sans-serif",
        style: "normal",
        weight: "medium",
      },
    };
  } else if (type === "D") {
    return {
      color: "C0EDFF",
      font: {
        family: "'Alata', sans-serif",
        style: "normal",
        weight: "medium",
      },
    };
  }
}
