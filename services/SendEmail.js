import nodemailer from "nodemailer";

// async..await is not allowed in global scope, must use a wrapper
export default async function sendEmail(email, type) {
  let transporter = nodemailer.createTransport({
    service: "gmail",
    host: "smtp.gmail.com",
    port: 587,
    secure: false,
    auth: {
      user: process.env.MY_EMAIL_ADDRESS,
      pass: process.env.MY_EMAIL_PASSWORD,
    },
  });

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: "quotes@islamipedia.com", // sender address
    to: email, // list of receivers
    subject: "Your Islamipedia Al-Qur'an/Hadith Quotes", // Subject line
    text: "Assalamualaikum brother/sister,\n\n This is your generated quotes you request before that I attached it below the email. Hope to see you using our service next time 😁 \n\n Regards \n\n Muhammad Haqqi Al Farizi", // plain text body
    attachments: [
      {
        filename: "quotes.png",
        path: type === "quran" ? "./my-quote-quran.png" : "./my-quote-hadith.png",
      },
    ],
  });
}
